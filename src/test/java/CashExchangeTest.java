import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dlunev on 2/14/17.
 */
public class CashExchangeTest {

    @Test
    public void change() throws Exception {

        Map<Integer, Integer> availableCash = new LinkedHashMap<>();
        availableCash.put(3, 10);
        availableCash.put(2, 10);
        availableCash.put(1, 10);

        CashExchange cashExchange = new CashExchange();
        List<List<Integer>> change = cashExchange.change(3, availableCash);

        Assert.assertEquals(3, change.size());
        Assert.assertArrayEquals(new Integer[] {1, 0, 0},
                change.get(0).toArray());
        Assert.assertArrayEquals(new Integer[] {0, 1, 1},
                change.get(1).toArray());
        Assert.assertArrayEquals(new Integer[] {0, 0, 3},
                change.get(2).toArray());
    }

    @Test
    public void changeLessOptions() throws Exception {

        Map<Integer, Integer> availableCash = new LinkedHashMap<>();
        availableCash.put(3, 2);
        availableCash.put(2, 2);
        availableCash.put(1, 2);

        CashExchange cashExchange = new CashExchange();
        List<List<Integer>> change = cashExchange.change(3, availableCash);

        Assert.assertEquals(2, change.size());
        Assert.assertArrayEquals(new Integer[] {1, 0, 0},
                change.get(0).toArray());
        Assert.assertArrayEquals(new Integer[] {0, 1, 1},
                change.get(1).toArray());
    }


    @Test
    public void change13() throws Exception {

        Map<Integer, Integer> availableCash = new LinkedHashMap<>();
        availableCash.put(20, 0);
        availableCash.put(10, 0);
        availableCash.put(5, 1);
        availableCash.put(2, 4);
        availableCash.put(1, 0);

        CashExchange cashExchange = new CashExchange();
        List<List<Integer>> change = cashExchange.change(8, availableCash);

        Assert.assertEquals(1, change.size());
        Assert.assertArrayEquals(new Integer[] {0, 0, 0, 4, 0},
                change.get(0).toArray());
    }

    @Test
    public void change13_2() throws Exception {

        Map<Integer, Integer> availableCash = new LinkedHashMap<>();
        availableCash.put(20, 0);
        availableCash.put(10, 0);
        availableCash.put(5, 2);
        availableCash.put(2, 0);
        availableCash.put(1, 3);

        CashExchange cashExchange = new CashExchange();
        List<List<Integer>> change = cashExchange.change(8, availableCash);

        Assert.assertEquals(1, change.size());
        Assert.assertArrayEquals(new Integer[] {0, 0, 1, 0, 3},
                change.get(0).toArray());
    }

    @Test
    public void changeNoOptions() throws Exception {

        Map<Integer, Integer> availableCash = new LinkedHashMap<>();
        availableCash.put(20, 0);
        availableCash.put(10, 1);
        availableCash.put(5, 0);
        availableCash.put(2, 1);
        availableCash.put(1, 1);

        CashExchange cashExchange = new CashExchange();
        List<List<Integer>> change = cashExchange.change(8, availableCash);

        Assert.assertEquals(0, change.size());
    }
}