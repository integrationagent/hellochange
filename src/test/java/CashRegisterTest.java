import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by dlunev on 2/14/17.
 */
public class CashRegisterTest {

    CashRegister cashRegister = new CashRegister();

    @Before
    public void clearRegister() throws Exception {
        cashRegister.clearRegister();
    }

    @Test
    public void checkSum() throws Exception {

        Integer[] bills = {1, 2, 3, 4, 5};

        cashRegister.putMoney(bills);
        assertEquals(68, cashRegister.calcAvailableCash());
    }

    @Test
    public void putMoney() throws Exception {

        Integer[] bills = {1, 2, 3, 4, 5};

        Map<Integer, Integer> result = cashRegister.putMoney(bills);
        assertArrayEquals(bills, result.values().toArray());
    }

    @Test
    public void putMoreBills() throws Exception {

        Integer[] bills = {1, 2, 3, 4, 5, 6, 7};
        Map<Integer, Integer> result = cashRegister.putMoney(bills);

        assertArrayEquals(new Integer[] {1, 2, 3, 4, 5},
                result.values().toArray());
    }

    @Test
    public void putLessBills() throws Exception {

        Integer[] bills = {1, 2, 3};

        Map<Integer, Integer> result = cashRegister.putMoney(bills);

        assertArrayEquals(new Integer[] {1, 2, 3, 0, 0},
                result.values().toArray());
    }

    @Test
    public void takeMoney() throws Exception {

        Integer[] deposit = {3, 3, 3, 3, 3};
        cashRegister.putMoney(deposit);

        Integer[] withdrawal = {1, 2, 3, 2, 1};
        Map<Integer, Integer> result = cashRegister.takeMoney(withdrawal);

        assertEquals(54, cashRegister.calcAvailableCash());
        assertArrayEquals(new Integer[] {2, 1, 0, 1, 2}, result.values().toArray());
    }

    @Test(expected = InSufficientFundsException.class)
    public void takeMoreBills() throws Exception {

        Integer[] deposit = {3, 3, 3, 3, 3};
        cashRegister.putMoney(deposit);

        Integer[] withdrawal = {0, 4, 0, 0, 0};
        cashRegister.takeMoney(withdrawal);
    }

    @Test(expected = InSufficientFundsException.class)
    public void takeMoreMoney() throws Exception {

        Integer[] deposit = {3, 3, 3, 3, 3};
        cashRegister.putMoney(deposit);

        Integer[] withdrawal = {4, 4, 4, 4, 4};
        cashRegister.takeMoney(withdrawal);
    }

    @Test
    public void change() throws Exception {

        Integer[] deposit = {1, 2, 3, 4, 5};
        cashRegister.putMoney(deposit);

        cashRegister.change(14);

        assertEquals(54, cashRegister.calcAvailableCash());
    }

    @Test(expected = InSufficientFundsException.class)
    public void changeNotEnough() throws Exception {

        Integer[] deposit = {1, 2, 3, 4, 5};
        cashRegister.putMoney(deposit);

        cashRegister.change(74);
    }
}