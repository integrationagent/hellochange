import java.util.*;

/**
 * Created by dlunev on 2/14/17.
 */
public class CashExchange {

    private List<List<Integer>> totalResult = new ArrayList<>();

    public List<List<Integer>> change(int amount, Map<Integer, Integer> cash){
        List<Integer> result = new ArrayList<>();
        change(result, amount, cash, 0);

        return toIndexes(totalResult, cash);
    }

    private List<Integer> change(List<Integer> result, int amount, Map<Integer, Integer> cash, int pos){
        if(amount == 0){
            totalResult.add(new ArrayList<>(result));
            return result;
        }

        Integer[] bills = cash.keySet().toArray(new Integer[0]);

        for(int i = pos; i < bills.length; i++){
            //Check if we have a bill to subtract from the amount and there is enough cash for specific denomination
            if(amount >= bills[i] && Collections.frequency(result, bills[i]) < cash.get(bills[i])){
                result.add(bills[i]);
                change(result, amount - bills[i], cash, i);
                result.remove(result.size() - 1);
            }
        }

        return result;
    }

    private List<List<Integer>> toIndexes(List<List<Integer>> result, Map<Integer, Integer> cash){

        Integer[] bills = cash.keySet().toArray(new Integer[0]);
        List<List<Integer>> indexedResult = new ArrayList<>();

        for (List<Integer> integers : result) {
            List<Integer> in = new ArrayList<>();

            for (Integer bill : bills) {
                in.add(Collections.frequency(integers, bill));
            }
            indexedResult.add(in);
        }

        return indexedResult;
    }
}
