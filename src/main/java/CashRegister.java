import java.util.*;

/**
 * Created by dlunev on 2/14/17.
 */
public class CashRegister {

    private Map<Integer, Integer> availableCash = new LinkedHashMap<>();

    private int[] bills = {20, 10, 5, 2, 1};

    private CashExchange cashExchange = new CashExchange();

    public Map<Integer, Integer> putMoney(Integer[] money) {

        money = normalizeBills(money);

        for (int i = 0; i < bills.length; i++) {
            availableCash.put(bills[i], money[i]);
        }

        return availableCash;
    }

    public Map<Integer, Integer> takeMoney(Integer[] money) throws InSufficientFundsException {

        if(Arrays.asList(money).stream().mapToInt(a -> a).sum() > calcAvailableCash()){
            throw new InSufficientFundsException();
        }

        normalizeBills(money);

        for (int i = 0; i < bills.length; i++) {
            if(availableCash.get(bills[i]) < money[i]){
                throw new InSufficientFundsException();
            }

            availableCash.put(bills[i], availableCash.get(bills[i]) - money[i]);
        }

        return availableCash;
    }

    public List<List<Integer>> change(Integer amount) throws InSufficientFundsException {

        if(amount > calcAvailableCash()){
            throw new InSufficientFundsException();
        }

        List<List<Integer>> change = cashExchange.change(amount, availableCash);

        if(!change.isEmpty()){
            takeMoney(change.get(0).toArray(new Integer[0]));
        }

        return change;
    }

    private Integer[] normalizeBills(Integer[] money) {

        if(money.length < bills.length){
            Integer[] expandedMoney = new Integer[bills.length];
            System.arraycopy(money, 0,  expandedMoney, 0, money.length);
            Arrays.fill(expandedMoney, money.length, bills.length, 0);

            return  expandedMoney;
        }

        return money;
    }

    public void clearRegister(){
        availableCash.clear();
    }

    public Map<Integer, Integer> getAvailableCash() {
        return availableCash;
    }

    public int calcAvailableCash() {
        return availableCash.entrySet().stream()
                .mapToInt(e -> e.getKey() * e.getValue())
                .sum();
    }
}
