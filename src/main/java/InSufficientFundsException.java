/**
 * Created by dlunev on 2/14/17.
 */
public class InSufficientFundsException extends Exception {

    public InSufficientFundsException(String message) {
        super(message);
    }

    public InSufficientFundsException() {
        super();
    }
}
